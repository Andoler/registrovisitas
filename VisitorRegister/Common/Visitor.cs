﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using VisitorRegister.src;

namespace VisitorRegister.Common
{
    public class Visitor
    {
        public string name { get; set; }
        public string dni { get; set; }
        public string company { get; set; }
        public Contact contact { get; set; }
        public string cardRegistered { get; set; }
        public bool stillInside { get; set; }

        public DateTime startDate { get; set; }
        public DateTime finishDate { get; set; }

        public Visitor()
        {
            name = "";
            dni = "";
            company = "";
            cardRegistered = "";
            stillInside = false;
            startDate = new DateTime();
            finishDate = new DateTime();

            contact = new Contact();
        }

        public Visitor(string _name, string _dni, string _company, string _contactname, DateTime _startDate, DateTime _finishDate, string _card, bool _stillinside)
        {
            name = _name;
            dni = _dni;
            company = _company;
            cardRegistered = "";
            
            startDate = _startDate;
            finishDate = _finishDate;

            if (finishDate > startDate)
                stillInside = false;
            else
                stillInside = true;

            cardRegistered = _card;
            stillInside = _stillinside;

            contact = SharedVars.contacts.getContactByName(_contactname);
        }
        public Visitor(string _name, string _dni, string _company, string _contactname, DateTime _startDate)
        {
            name = _name;
            dni = _dni;
            company = _company;
            cardRegistered = "";

            startDate = _startDate;

            if (finishDate > startDate)
                stillInside = false;
            else
                stillInside = true;

            contact = SharedVars.contacts.getContactByName(_contactname);
        }

        public static List<Visitor> LoadConfigXmlFromDisk(String path)
        {
            XmlSerializer xmlSerial = new XmlSerializer(typeof(List<Visitor>));
            List<Visitor> actualVisitors = new List<Visitor>();
            using (XmlReader reader = XmlReader.Create(path))
            {
                actualVisitors = (List<Visitor>)xmlSerial.Deserialize(reader);
            }
            return actualVisitors;
        }

        public static void WriteToDisk()
        {
            XmlSerializer serialiser = new XmlSerializer(typeof(List<Visitor>));

            // Create the TextWriter for the serialiser to use
            TextWriter filestream = new StreamWriter(Environment.CurrentDirectory + "\\ActualVisitors.xml");

            //write to the file
            serialiser.Serialize(filestream, SharedVars.actualVisitorTotal);

            // Close the file
            filestream.Close();
            //XmlSerializer xsSubmit = new XmlSerializer(typeof(List<Visitor>));
            //var xml = "";

            //using (var sww = new StringWriter())
            //{
            //    using (XmlWriter writer = XmlWriter.Create(sww))
            //    {
            //        xsSubmit.Serialize(writer, this);
            //        xml = sww.ToString();
            //        XmlDocument xdoc = new XmlDocument();
            //        xdoc.LoadXml(xml);
            //        xdoc.Save(path);
            //    }
            //}
        }
    }
}
