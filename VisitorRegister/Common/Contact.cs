﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorRegister.src
{
    public class Contact
    {
        public string name;
        public string extension;
        public string phone;
        public string email;

        public Contact()
        {

        }
    }
}
