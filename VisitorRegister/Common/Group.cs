﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorRegister.Common
{
    public class Group
    {
        public string company { get; set; }
        public List<Visitor> visitors { get; set; }

        public Group()
        {
            company = "";
            visitors = new List<Visitor>();
        }
    }
}
