﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisitorRegister.src;

namespace VisitorRegister.Common
{
    public class SharedVars
    {
        private static object locker = new object();
        public static bool isReaderConnected
        {
            get
            {
                lock (locker)
                {
                    return _isReaderConnected;
                }
            }
            set
            {
                lock (locker)
                {
                    _isReaderConnected = value;
                }
            }
        }
        private static bool _isReaderConnected = false;

        public static int readerID
        {
            get
            {
                lock (locker)
                {
                    return _readerID;
                }
            }
            set
            {
                lock (locker)
                {
                    _readerID = value;
                }
            }
        }
        private static int _readerID = 0;

        public static Visitor actualVisitor
        {
            get
            {
                lock (locker)
                {
                    return _actualVisitor;
                }
            }
            set
            {
                lock (locker)
                {
                    _actualVisitor = value;
                }
            }
        }
        private static Visitor _actualVisitor = null;

        public static List<Visitor> actualVisitorTotal
        {
            get
            {
                lock (locker)
                {
                    return _actualVisitorTotal;
                }
            }
            set
            {
                lock (locker)
                {
                    _actualVisitorTotal = value;
                }
            }
        }
        private static List<Visitor> _actualVisitorTotal = new List<Visitor>();

        public static LoadContacts contacts
        {
            get
            {
                lock (locker)
                {
                    return _contacts;
                }
            }
            set
            {
                lock (locker)
                {
                    _contacts = value;
                }
            }
        }
        private static LoadContacts _contacts = new LoadContacts();

        public static IndividualForm.ProcessDoneEvent ProcessDoneEvent
        {
            get
            {
                lock (locker)
                {
                    return _processDone;
                }
            }
            set
            {
                _processDone = value;
            }
        }

        private static IndividualForm.ProcessDoneEvent _processDone = null;

        public static SQLManager connection
        {
            get
            {
                lock(locker)
                {
                    return _connection;
                }
            }
            set
            {
                _connection = value;
            }
        }
        private static SQLManager _connection = null;

        public static Configuration config
        {
            get
            {
                lock (locker)
                {
                    return _config;
                }
            }
            set
            {
                _config = value;
            }
        }
        private static Configuration _config = null;
    }
}
