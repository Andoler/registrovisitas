﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using VisitorRegister.Common;

namespace VisitorRegister
{
    public partial class CardReaderForm : System.Windows.Forms.Form
    {
        string mode;
        public string cardRead { get; set; }
        public CardReaderForm(string _mode)
        {
            this.WindowState = FormWindowState.Maximized;
            mode = _mode;
            cardRead = "";
            InitializeComponent();

            cardReaderControl1.bgWorker.RunWorkerCompleted += HideCardReader;

            this.FormClosing += CardReaderForm_FormClosing;
        }

        private void CardReaderForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            cardReaderControl1.closing = true;
            cardReaderControl1.state = 0;
        }

        private void HideCardReader(object sender, RunWorkerCompletedEventArgs e)
        {
            if (cardReaderControl1.closing)
            {
                /*this.pictureBox1.Visible = true;
                Thread.Sleep(1000);
                this.pictureBox1.Visible = false;*/
                this.Close();
            }
            MessageBoxForm done;
           // Done done = new Done();
            switch (mode)
            {
                case "LOGIN":
                    if (CheckData(this.cardReaderControl1.cardData[0].ToString()))
                    {
                        done = new MessageBoxForm("No olvide  usar la mascarilla durante toda la visita y limpiarse las manos antes y después de su visita.");
                        done.ShowDialog();
                        CheckCalendarData(this.cardReaderControl1.cardData[0].ToString());
                    }
                    break;
                case "INFO":
                    ShowInfoData(this.cardReaderControl1.cardData[0].ToString());
                    break;
                case "LOGOUT":
                    done = new MessageBoxForm("Gracias por su visita y no olvide limpiarse las manos.");
                    done.ShowDialog();
                    Checkout(this.cardReaderControl1.cardData[0].ToString());
                    break;
                case "USER":
                    //TODO REGISTER CALENDAR FOR EKIDE USERS
                    break;
            }

            cardRead = this.cardReaderControl1.cardData[0].ToString();
            this.Close();
        }

        private bool CheckData(string v)
        {
            foreach(Visitor vis in SharedVars.actualVisitorTotal)
            {
                if(v == vis.cardRegistered)
                {
                    MetroFramework.MetroMessageBox.Show(this, "Card already registered");
                    return false;
                }
            }
            return true;
        }

        private void Checkout(string card)
        {
            foreach (Visitor v in SharedVars.actualVisitorTotal)
            {
                if(v.cardRegistered == card)
                {
                    v.finishDate = DateTime.Now;
                    v.cardRegistered = "";
                    v.stillInside = false;

                    SharedVars.connection.updateVisitor(v, "FinishDate");

                    //SharedVars.excel.visitors.Add(v);
                    //SharedVars.excel.exportDatabaseExcel(v);
                    SharedVars.actualVisitorTotal.Remove(v);
                    Visitor.WriteToDisk();
                    MetroFramework.MetroMessageBox.Show(this, "Thank you for your visit", "THANKS");
                    return;
                }
            }
            
            MetroFramework.MetroMessageBox.Show(this, "No visitor found with that card", "ERROR");

        }

        private void ShowInfoData(string card)
        {
            foreach (Visitor v in SharedVars.actualVisitorTotal)
            {
                if(v.cardRegistered == card)
                {
                    InfoData info = new InfoData(v);
                    info.ShowDialog();
                    return;
                }
            }

            MetroFramework.MetroMessageBox.Show(this, "No visitor found with that card", "ERROR");

        }

        private void CheckCalendarData(string v)
        {
            //if(File.Exists("Calendar.xlsx"))
            //{

            //}
            //else
            //{
            //    MetroFramework.MetroMessageBox.Show(this, "No se han encontrado visitas a registrar con este usuario", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}

        }

        private void CardReaderForm_Load(object sender, EventArgs e)
        {
            this.cardReaderControl1.Start();
        }

    }
}
