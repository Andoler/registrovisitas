﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;
using VisitorRegister.src;

namespace VisitorRegister
{
    public partial class AdminForm 
    {
        List<Visitor> visitorToDelete;
        BackgroundWorker bg;
        DataTable dt;
        //string path = "C:/Users/ekide/Desktop/";
        //string file = "VisitorsDatabase.xlsx";
        //long lenght = 0;
        int originalMetroHeight;
        bool finish;
        private Form1 form1;

        // List<string> dnisToDelete;

        public AdminForm()
        {
            
            
        }

        public AdminForm(Form1 form1)
        {
            this.form1 = form1;
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
            bg = new BackgroundWorker();
            bg.DoWork += Bg_DoWork;
            visitorToDelete = new List<Visitor>();
            bg.RunWorkerCompleted += Bg_RunWorkerCompleted;
            finish = false;
            if (metroContextMenu1.Items != null)
                metroContextMenu1.Items.Clear();
            metroContextMenu1.Items.Add("Delete", null, onClick);
            originalMetroHeight = metroGrid1.Height;
        }

        private void Bg_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            bg.Dispose();
        }

        private void Bg_DoWork(object sender, DoWorkEventArgs e)
        {
            object date = SharedVars.connection.connection.GetLifetimeService();
            while (!finish)
            {
                try
                {
                    Thread.Sleep(5000);
                    SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Modified", SharedVars.connection.connection);

                    SqlDataReader reader = sqlCommand.ExecuteReader();

                    while (reader.Read())
                    {
                        object[] data = new object[reader.FieldCount];
                        reader.GetValues(data);
                        int modified = Convert.ToInt32(data[0]);
                        if (modified == 1)
                        {
                            RefreshData();

                            SharedVars.connection.modifiedOn(false);

                        }
                    }

                    reader.Close();
                }
                catch(Exception ex)
                {

                }
            }
        }

        public void RefreshData()
        {
            BeginInvoke((MethodInvoker)delegate
            {
                LoadByDateDataGrid();
            });   
        }

        private void AdminForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            finish = true;
        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
           
            //Load actual Database
            //if(File.Exists(path + file))
            //{
            //    LoadData(path + file);
            //    //LoadDatagrid(false);
            //}
            metroDateTime1.Value = DateTime.Now;
            metroDateTime2.Value = DateTime.Now;

            metroDateTime3.Value = DateTime.Now;
            metroDateTime4.Value = DateTime.Now;

            LoadByDateDataGrid();
            if(metroGrid1.Rows.Count == 0)
                bunifuCustomLabel1.Text = "0";
            else
                bunifuCustomLabel1.Text = (this.metroGrid1.Rows.Count - 1).ToString();

            bg.RunWorkerAsync();
        }

        //private void LoadData(string filename)
        //{
        //    SpreadsheetLight.SLDocument sLDocument = new SpreadsheetLight.SLDocument(filename);

        //    if (visitors.Count != 0)
        //        visitors.Clear();

        //    int i = 1;
        //    do
        //    {
        //        i++;

        //        string name = sLDocument.GetCellValueAsString(i, 1);
        //        string dni = sLDocument.GetCellValueAsString(i, 2);
        //        string company = sLDocument.GetCellValueAsString(i, 3);
        //        string contactname = sLDocument.GetCellValueAsString(i, 4);
        //        DateTime checkin = sLDocument.GetCellValueAsDateTime(i, 5);
        //        DateTime checkout = sLDocument.GetCellValueAsDateTime(i, 6);
        //        string cardRegistered = sLDocument.GetCellValueAsString(i, 7);

        //        if (checkout == new DateTime() && checkin.Date == DateTime.Now.Date)
        //            visitors.Add(new Visitor(name, dni, company, contactname, checkin, checkout, cardRegistered, true));
        //        else
        //            visitors.Add(new Visitor(name, dni, company, contactname, checkin, checkout, "", false));

        //    }
        //    while (sLDocument.GetCellValueAsString(i+1, 1) != "");

        //    foreach (Visitor v in SharedVars.actualVisitorTotal)
        //        visitors.Add(v);


        //    metroGrid1.DataSource = visitors;
        //}

        private void metroDateTime2_ValueChanged(object sender, EventArgs e)
        {
            if (metroDateTime2.Value.Date >= metroDateTime1.Value.Date)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    LoadByDateDataGrid();
                });
            }
        }

       
        private void metroDateTime1_ValueChanged(object sender, EventArgs e)
        {
            if(metroDateTime2.Value.Date >= metroDateTime1.Value.Date)
            {
                BeginInvoke((MethodInvoker)delegate
                {
                    LoadByDateDataGrid();
                });
            }
        }

        private void LoadByDateDataGrid()
        {
            int actualperson = 0;
            try
            {
                //SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Visitas where (StartDate between '" + metroDateTime1.Value.ToShortDateString() + "' and '" + metroDateTime2.Value.ToShortDateString() + "' ) or (StartDate between '" + metroDateTime1.Value.ToShortDateString()+ "' and '"+ metroDateTime1.Value.ToShortDateString()+" 23:59:59' And FinishDate IS NULL)", SharedVars.connection.connection);
                String sql = "SELECT* FROM Visitas where(StartDate between @startDate and @endDate ) or(StartDate between @startDate and @endDay And FinishDate IS NULL)";
                //sql = "";
                SqlCommand command = new SqlCommand(sql, SharedVars.connection.connection);

                string date1 = metroDateTime1.Value.ToShortDateString();
                DateTime date3 = Convert.ToDateTime(date1);
                //date1 = date1.AddHours(date1.Hour - date1.Hour);
                //date1 = date1.AddMinutes(date1.Minute - date1.Minute);
                //date1 = date1.AddSeconds(date1.Second - date1.Second);

                command.Parameters.Add("@startDate", SqlDbType.DateTime).Value = date3.ToString("yyyy-MM-dd HH:mm:ss");
                command.Parameters.Add("@endDate", SqlDbType.DateTime).Value = metroDateTime2.Value.ToString("yyyy-MM-dd HH:mm:ss");

                DateTime date = metroDateTime1.Value;
                date = date.AddHours(23 - date.Hour);
                date = date.AddMinutes(59 - date.Minute);
                date = date.AddSeconds(59 - date.Second);
                command.Parameters.Add("@endDay", SqlDbType.DateTime).Value = date.ToString("yyyy-MM-dd HH:mm:ss");
                SqlDataAdapter da = new SqlDataAdapter(command);
                dt = new DataTable();
                da.Fill(dt);
                
                metroGrid1.DataSource = dt;

                //List<Visitor> visitors = new List<Visitor>();
                //SqlCommand sqlCommand = new SqlCommand("SELECT * FROM EKBisita.dbo.Visitas where StartDate between '" + metroDateTime1.Value.Date.ToString() + "' and '" + metroDateTime2.Value.Date.ToString() + "'", SharedVars.connection.connection);
                //SqlDataReader reader = sqlCommand.ExecuteReader();

                //while (reader.Read())
                //{
                //    object[] data = new object[reader.FieldCount];
                //    reader.GetValues(data);
                //    Visitor v = new Visitor(data[1].ToString(), data[0].ToString(), data[2].ToString(), data[3].ToString(), (DateTime)data[4], (DateTime)data[5], "", false);
                //    visitors.Add(v);
                //}
                //reader.Close();

                SqlCommand sqlCommandAcutalPerson = new SqlCommand("SELECT * FROM Visitas where StartDate between '" + DateTime.Now.Date.ToString() + "' and '" + DateTime.Now.ToString() + "' And FinishDate is NULL", SharedVars.connection.connection);
                SqlDataReader readerNow = sqlCommandAcutalPerson.ExecuteReader();

                while (readerNow.Read())
                {
                    object[] data = new object[readerNow.FieldCount];
                    readerNow.GetValues(data);
                    actualperson++;
                }
                readerNow.Close();
                //metroGrid1.DataSource = null;
                //metroGrid1.DataSource = visitors;
                bunifuCustomLabel1.Text = actualperson.ToString();
                resizeMetroGrid();
            }
            catch(Exception Ex)
            {

            }
        }


        //EXPORTAR
        private void bunifuTileButton1_Click(object sender, EventArgs e)
        {
            SaveFileDialog op = new SaveFileDialog();
            
            op.AddExtension = true;
            op.DefaultExt = "xlsx";
            string date = DateTime.Now.ToShortDateString().Replace("/", "_");
            op.InitialDirectory = "C:\\Users\\" + Environment.UserName + "\\Desktop\\";
            op.Filter = "Excel files (*.xlsx)|*.xlsx";
            op.FileName = "Visitors_" + date + ".xlsx";
            DialogResult dR = op.ShowDialog();
            if(dR == DialogResult.OK)
            {
                if (SharedVars.connection.exportExcel(this.metroDateTime3.Value, this.metroDateTime4.Value, op.FileName))
                {
                    MetroFramework.MetroMessageBox.Show(this, "Excel exportado correctamente.", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void metroGrid1_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            if (e.Button == MouseButtons.Right)
            {
                if (e.RowIndex >= 0)
                {
                    Point n = new Point(MousePosition.X, MousePosition.Y);
                    Point p = PointToScreen(e.Location);

                    metroContextMenu1.Show(n);
                }
            }
        }

        private void onClick(object sender, EventArgs e)
        {

            foreach (DataGridViewRow r in metroGrid1.SelectedRows)
            {
                DataRowView v = (DataRowView)r.DataBoundItem;
                Visitor visitor; 
                if (v.Row.ItemArray[5].ToString() == "")
                    visitor = new Visitor(v.Row.ItemArray[1].ToString(), v.Row.ItemArray[0].ToString(), v.Row.ItemArray[2].ToString(), v.Row.ItemArray[3].ToString(), (DateTime)v.Row.ItemArray[4]);
                else
                    visitor = new Visitor(v.Row.ItemArray[1].ToString(), v.Row.ItemArray[0].ToString(), v.Row.ItemArray[2].ToString(), v.Row.ItemArray[3].ToString(), (DateTime)v.Row.ItemArray[4], (DateTime)v.Row.ItemArray[5], "", false);

                visitorToDelete.Add(visitor);
                //auxDelete.Add((Visitor)r.DataBoundItem);
            }

            metroGrid1.Rows.Remove(metroGrid1.SelectedRows[0]);
            //List<Visitor> auxDelete = new List<Visitor>();
            //foreach (DataGridViewRow r in metroGrid1.SelectedRows)
            //{
            //    auxDelete.Add((Visitor)r.DataBoundItem);
            //}

            //metroGrid1.DataSource = null;

            //foreach (Visitor v in auxDelete)
            //    visitors.Remove(v);

            //metroGrid1.DataSource = visitors;

            resizeMetroGrid();
        }

        //RECARGAR
        private void bunifuTileButton2_Click(object sender, EventArgs e)
        {
            //Reload changes
            //LoadData(path + file);
            visitorToDelete.Clear();
            LoadByDateDataGrid();
            
        }

        //GUARDAR
        private void bunifuTileButton3_Click(object sender, EventArgs e)
        {
            foreach(Visitor v in visitorToDelete)
            {
                SharedVars.connection.deleteVisitor(v);
            }
            foreach (DataGridViewRow r in metroGrid1.Rows)
            {
                DataRowView v = (DataRowView)r.DataBoundItem; Visitor visitor;
                if (v.Row.ItemArray[5].ToString() == "")
                    visitor = new Visitor(v.Row.ItemArray[1].ToString(), v.Row.ItemArray[0].ToString(), v.Row.ItemArray[2].ToString(), v.Row.ItemArray[3].ToString(), (DateTime)v.Row.ItemArray[4]);
                else
                    visitor = new Visitor(v.Row.ItemArray[1].ToString(), v.Row.ItemArray[0].ToString(), v.Row.ItemArray[2].ToString(), v.Row.ItemArray[3].ToString(), (DateTime)v.Row.ItemArray[4], (DateTime)v.Row.ItemArray[5], "", false);

                SharedVars.connection.updateVisitorAll(visitor);
            }

            
            ////Save changes
            //List<Visitor> auxVisitors = new List<Visitor>(SharedVars.excel.visitors);
            //foreach (string s in dnisToDelete)
            //{
            //    foreach (Visitor v in auxVisitors)
            //    {
            //        if (v.dni == s)
            //        {
            //            SharedVars.excel.visitors.Remove(v);
            //            break;
            //        }
            //    }
            //}

            //SharedVars.excel.clearDatabaseExcel();
            //SharedVars.excel.exportDatabaseExcel();

            //LoadData(path + file);
            //LoadByDateDataGrid(false);
        }

        private void metroGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                List<int> indexes = new List<int>();

               // List<Visitor> auxDelete = new List<Visitor>();
                foreach(DataGridViewRow r in metroGrid1.SelectedRows)
                {
                    DataRowView v = (DataRowView)r.DataBoundItem;
                    Visitor visitor;
                    if (v.Row.ItemArray[5].ToString() == "")
                        visitor = new Visitor(v.Row.ItemArray[1].ToString(), v.Row.ItemArray[0].ToString(), v.Row.ItemArray[2].ToString(), v.Row.ItemArray[3].ToString(), (DateTime)v.Row.ItemArray[4]);
                    else
                        visitor = new Visitor(v.Row.ItemArray[1].ToString(), v.Row.ItemArray[0].ToString(), v.Row.ItemArray[2].ToString(), v.Row.ItemArray[3].ToString(), (DateTime)v.Row.ItemArray[4], (DateTime) v.Row.ItemArray[5], "", false);
                    
                    visitorToDelete.Add(visitor);

                    indexes.Add(r.Index);

                    //auxDelete.Add((Visitor)r.DataBoundItem);
                }

                //for(int i = 0; i< metroGrid1.SelectedRows.Count; i++)
                //{
                //    metroGrid1.Rows.Remove(metroGrid1.SelectedRows[i]);
                //    i--;
                //}


                indexes.Sort();

                for(int i = indexes.Count -1; i >= 0; i--)
                {
                    dt.Rows[indexes[i]].Delete();
                }
                //metroGrid1.DataSource = null;
                //dnisToDelete.Add()
                //foreach (Visitor v in auxDelete)
                //    visitors.Remove(v);

                //metroGrid1.DataSource = visitors;
            }
        }

        public void resizeMetroGrid()
        {
            metroGrid1.Height = metroGrid1.ColumnHeadersHeight + (metroGrid1.Rows.Count * metroGrid1.RowTemplate.Height);
            if (metroGrid1.Height > originalMetroHeight)
            {
                metroGrid1.Height = originalMetroHeight;
            }
        }

        private void backButton_MouseEnter(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._18;
        }

        private void backButton_MouseLeave(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void configurationButton_Click(object sender, EventArgs e)
        {
            ConfigForm cF = new ConfigForm();
            cF.ShowDialog();

            if (!cF.isCancelled)
            {
                SharedVars.config = Configuration.LoadConfigXmlFromDisk(Environment.CurrentDirectory + "\\ConfigOpt.xml");
                StartDatabaseConnection();
                //ConnectToExchangeService();
                if (form1.bgWorkerContacts.IsBusy != true)
                    form1.bgWorkerContacts.RunWorkerAsync();
            }
        }
        private void StartDatabaseConnection()
        {
            SharedVars.connection = new SQLManager();
            //object date = SharedVars.connection.connection.GetLifetimeService();
        }
    }
}
