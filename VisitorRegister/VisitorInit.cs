﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisitorRegister
{
    public partial class VisitorInit : System.Windows.Forms.Form
    {
        
        public VisitorInit()
        {
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
            
        }

        private void Parent_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
            this.Close();
        }

        private void VisitorInit_Load(object sender, EventArgs e)
        {
        }

        private void individualButton_Click(object sender, EventArgs e)
        {
            IndividualForm individualForm = new IndividualForm();
            individualForm.FormClosed += Parent_FormClosed;
            individualForm.Show();
            this.Hide();
        }

        private void grupalButton_Click(object sender, EventArgs e)
        {
            GroupForm groupForm = new GroupForm();
            groupForm.FormClosed += Parent_FormClosed;
            groupForm.Show();
            this.Hide();
        }

        private void individualButton_MouseEnter(object sender, EventArgs e)
        {
            this.individualButton.BackgroundImage = global::VisitorRegister.Properties.Resources._9;
            this.individualButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
        }

        private void individualButton_MouseLeave(object sender, EventArgs e)
        {
            this.individualButton.BackgroundImage = global::VisitorRegister.Properties.Resources._7;
            this.individualButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
        }

        private void grupalButton_MouseEnter(object sender, EventArgs e)
        {
            this.grupalButton.BackgroundImage = global::VisitorRegister.Properties.Resources._20;
            this.grupalButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
        }

        private void grupalButton_MouseLeave(object sender, EventArgs e)
        {
            this.grupalButton.BackgroundImage = global::VisitorRegister.Properties.Resources._8;
            this.grupalButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExistingUserFrm eUf = new ExistingUserFrm();
            eUf.FormClosed += Parent_FormClosed;
            eUf.Show();
            this.Hide();

        }
        private void button1_MouseEnter(object sender, EventArgs e)
        {
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
        }
        private void backButton_MouseEnter(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._18;
        }

        private void backButton_MouseLeave(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
