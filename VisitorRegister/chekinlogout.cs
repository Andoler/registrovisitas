﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisitorRegister
{
    public partial class chekinlogout : System.Windows.Forms.Form
    {
        public chekinlogout()
        {
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
        }

        private void checkinButton_Click(object sender, EventArgs e)
        {
            VisitorInit visitorInit = new VisitorInit();
            visitorInit.FormClosed += Parent_FormClosed;
            visitorInit.Show();
            this.Hide();

        }

        private void infoButton_Click(object sender, EventArgs e)
        {
            
            CardReaderForm card = new CardReaderForm("INFO");
            card.FormClosed += Parent_FormClosed;
            card.Show();
            this.Hide();

        }

        private void checkoutButton_Click(object sender, EventArgs e)
        {
            CardReaderForm card = new CardReaderForm("LOGOUT");
            card.FormClosed += Parent_FormClosed;
            card.Show();
            this.Hide();
        }

        private void Parent_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void euskeraButton_Click(object sender, EventArgs e)
        {

        }

        private void spanishButton_Click(object sender, EventArgs e)
        {

        }

        private void englishButton_Click(object sender, EventArgs e)
        {

        }

        private void chekinlogout_Load(object sender, EventArgs e)
        {
            //this.checkinButton.Location = new Point ( 1920 / 2 - this.checkinButton.Width - 10, this.Height/2 - (this.checkinButton.Height/2));
            //this.checkoutButton.Location = new Point(1920 / 2 + 10, this.Height / 2 - (this.checkoutButton.Height / 2));
        }

        private void checkinButton_MouseEnter(object sender, EventArgs e)
        {
            this.checkinButton.BackgroundImage = global::VisitorRegister.Properties.Resources._19;
            this.checkinButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
        }

        private void checkinButton_MouseLeave(object sender, EventArgs e)
        {
            this.checkinButton.BackgroundImage = global::VisitorRegister.Properties.Resources._1;
            this.checkinButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
        }

        private void checkoutButton_MouseEnter(object sender, EventArgs e)
        {
            this.checkoutButton.BackgroundImage = global::VisitorRegister.Properties.Resources._6;
            this.checkoutButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
            //resources.ApplyResources(this.checkoutButton, "checkoutButton");
        }

        private void checkoutButton_MouseLeave(object sender, EventArgs e)
        {
            this.checkoutButton.BackgroundImage = global::VisitorRegister.Properties.Resources._2;
            this.checkoutButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            //resources.ApplyResources(this.checkoutButton, "checkoutButton");
        }

        private void infoButton_MouseEnter(object sender, EventArgs e)
        {
            this.infoButton.BackgroundImage = global::VisitorRegister.Properties.Resources._21;
        }

        private void infoButton_MouseLeave(object sender, EventArgs e)
        {
            this.infoButton.BackgroundImage = global::VisitorRegister.Properties.Resources._4_1;
        }

        private void backButton_MouseEnter(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._18;
        }

        private void backButton_MouseLeave(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
