﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;
using VisitorRegister.src;
using System.Threading;
using System.Management;
using System.IO.Ports;
using System.IO;
using SpreadsheetLight;
using System.Net.Mail;
using System.Net;
using Microsoft.Exchange.WebServices.Data;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Data.SqlClient;

namespace VisitorRegister
{
    public partial class Form1 : System.Windows.Forms.Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]

        private static extern IntPtr CreateRoundRect
            (
            int nLeftRect,
            int nTopRect,
            int nRightRect,
            int nBottomRect,
            int nWidthEllipse,
            int nHeightEllipse
            );


        [DllImport("umf32.DLL", EntryPoint = "fw_init")]
        public static extern Int32 fw_init(Int16 port, Int32 baud);
        [DllImport("umf32.DLL", EntryPoint = "fw_exit")]
        public static extern Int32 fw_exit(Int32 icdev);
        [DllImport("umf32.DLL", EntryPoint ="fw_getver")]
        public static extern int fw_getver(int icdev, byte[] buff);

        public BackgroundWorker bgWorker;
        public BackgroundWorker bgWorkerContacts;
        bool closing;

        public Form1()
        {
            InitializeComponent();

            SharedVars.config = Configuration.LoadConfigXmlFromDisk(Environment.CurrentDirectory + "\\ConfigOpt.xml");

            bgWorker = new BackgroundWorker();
            bgWorker.DoWork += BgWorker_DoWork;
            //bgWorker.RunWorkerCompleted += BgWorker_RunWorkerCompleted;

            bgWorkerContacts = new BackgroundWorker();
            bgWorkerContacts.DoWork += BgWorkerContacts_DoWork;
            bgWorkerContacts.RunWorkerCompleted += BgWorkerContacts_RunWorkerCompleted;
            closing = false;

            this.passwordControl1.textBox1.PasswordChar = '*';
            this.passwordControl1.textBox1.UseSystemPasswordChar = true;
            this.visitorButton.Enabled = false;

            Update();
        }

        private void BgWorkerContacts_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.visitorButton.Enabled = true;
            pictureBox4.BackgroundImage = Image.FromFile(".\\Resources\\circulo-verde.png");
            //MetroFramework.MetroMessageBox.Show(this, "Contacts loaded correctly");
        }

        private void BgWorkerContacts_DoWork(object sender, DoWorkEventArgs e)
        {
            //this.visitorButton.Enabled = false;
            ConnectToExchangeService();
        }

        private void StartDatabaseConnection()
        {
            SharedVars.connection = new SQLManager();
            //object date = SharedVars.connection.connection.GetLifetimeService();
        }

        private static void ConnectToExchangeService()
        {
            Exchange ex = new Exchange();
            if (ex.checkCredentials())
            {
                LoadContacts lC = new LoadContacts();
                //lC.contacts = ex.retrieveContacts();
                SharedVars.contacts = lC;
                SharedVars.contacts.contacts = ex.retrieveContacts();
            }
            else
            {
                Console.WriteLine("no se han validado las credenciales y no se an introducido los contactos");
            }

        }

        
        
        /*private void BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //Free Visitor
            //CloseConnectionCardReader();
        }*/

        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            ConnectCardReader();
        }


        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CardReaderForm card = new CardReaderForm("EKIDEUSER");
            card.FormClosed += Parent_FormClosed;
            card.Show();
            this.Hide();
        }


        private void loginButton_Click(object sender, EventArgs e)
        {
            try
            {
                ExchangeService service = new ExchangeService();// ExchangeVersion.Exchange2007_SP1);
                service.Credentials = new WebCredentials(userControl11.textBox1.Text, passwordControl1.textBox1.Text, "IKUTIXO");
                service.AutodiscoverUrl(userControl11.textBox1.Text + "@ekide.es");

                AdminForm admin = new AdminForm(this);
                admin.FormClosed += Parent_FormClosed;
                admin.Show();
                this.Hide();
            }
            catch (Exception ex)
            {
                MetroFramework.MetroMessageBox.Show(this, "El usuario o contraseña no son correctos." + ex, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally { 
            userControl11.textBox1.Text = "Username";
            passwordControl1.textBox1.Text = "Password";
            }
        }

        private void visitorButton_Click(object sender, EventArgs e)
        {
            chekinlogout chekinlogout = new chekinlogout();
            chekinlogout.FormClosed += Parent_FormClosed;
            chekinlogout.Show();
            this.Hide();
            userControl11.textBox1.Text = "Username";
            passwordControl1.textBox1.Text = "Password";
        }

        private void Parent_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            bgWorker.RunWorkerAsync();
            bgWorkerContacts.RunWorkerAsync();
            //string filename = SharedVars.config.contactPath;
            //LoadContacts contacts = new LoadContacts(filename);
            //contacts.LoadAllData();

            //SharedVars.contacts = contacts;

            //ConnectCardReader();
            //if (!SharedVars.excel.LoadExcel())
            //    MetroFramework.MetroMessageBox.Show(this, "Visitor database not found", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            if (File.Exists(Environment.CurrentDirectory + "\\ActualVisitors.xml"))
                SharedVars.actualVisitorTotal = Visitor.LoadConfigXmlFromDisk(Environment.CurrentDirectory + "\\ActualVisitors.xml");

        }


        private void ConnectCardReader()
        {
            Byte[] revbuf = new Byte[64];

            while (!closing)
            {
                if (LookUpUsbDeviceAddresses())
                {
                    if (SharedVars.isReaderConnected == false)
                    {
                        Thread.Sleep(5000);
                        SharedVars.isReaderConnected = true;

                        while(SharedVars.readerID <= 0)
                        {
                            int val = fw_init(100, 0);
                            SharedVars.readerID = val;
                            closing = true;
                        }

                        pictureBox2.BackgroundImage = Image.FromFile(".\\Resources\\connected.png");
                        //notifyIcon1.Icon = new Icon("..\\..\\Resources\\connected.ico");
                        //notifyIcon1.Text = "Card Reader Connected";
                    }
                }
                //else
                //{
                //    if (SharedVars.isReaderConnected == true)
                //    {
                //        if (SharedVars.readerID > 0)
                //            fw_exit(SharedVars.readerID);

                //        SharedVars.readerID = 0;
                //        SharedVars.isReaderConnected = false;
                //        pictureBox2.BackgroundImage = Image.FromFile(".\\Resources\\disconnected.png");
                //        //notifyIcon1.Icon = new Icon("..\\..\\Resources\\disconnected.ico");
                //        //notifyIcon1.Text = "Card Reader Disconnected";
                //    }
                //}
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            bgWorker.WorkerSupportsCancellation = true;
            bgWorkerContacts.WorkerSupportsCancellation = true;
            if (bgWorker.IsBusy == true)
            {
                bgWorker.CancelAsync();
            }

            if (bgWorkerContacts.IsBusy == true)
            {
                bgWorkerContacts.CancelAsync();
            }
            closing = true;

            CloseConnectionCardReader();
        }

        private void CloseConnectionCardReader()
        {
            // Close reader
            
            if (SharedVars.readerID > 0)
                fw_exit(SharedVars.readerID);

            SharedVars.readerID = 0;

            SharedVars.isReaderConnected = false;
        }

        public static bool LookUpUsbDeviceAddresses()
        {
            // this query gets the addressing information for connected USB devices
            ManagementObjectCollection usbDeviceAddressInfo = QueryMi(@"Select * from Win32_USBControllerDevice");
            
            List<string> usbDeviceAddresses = new List<string>();

            foreach (var device in usbDeviceAddressInfo)
            {
                string curPnpAddress = (string)device.GetPropertyValue("Dependent");
                // split out the address portion of the data; note that this includes escaped backslashes and quotes
                curPnpAddress = curPnpAddress.Split(new String[] { "DeviceID=" }, 2, StringSplitOptions.None)[1];

                usbDeviceAddresses.Add(curPnpAddress);

                if (curPnpAddress.Contains("VID_0471&PID_A112"))
                    return true;
            }

            return false;
        }

        // run a query against Windows Management Infrastructure (MI) and return the resulting collection
        public static ManagementObjectCollection QueryMi(string query)
        {
            ManagementObjectSearcher managementObjectSearcher = new ManagementObjectSearcher(query);
            ManagementObjectCollection result = managementObjectSearcher.Get();

            managementObjectSearcher.Dispose();
            return result;
        }

        private void configurationButton_Click(object sender, EventArgs e)
        {
            ConfigForm cF = new ConfigForm();
            cF.ShowDialog();

            if (!cF.isCancelled)
            {
                SharedVars.config = Configuration.LoadConfigXmlFromDisk(Environment.CurrentDirectory + "\\ConfigOpt.xml");
                StartDatabaseConnection();
                //ConnectToExchangeService();
                if(bgWorkerContacts.IsBusy != true)
                    bgWorkerContacts.RunWorkerAsync();
            }
        }

        private void alarmButton_Click(object sender, EventArgs e)
        {
            EmailService email = new EmailService();
            List<string> names = SharedVars.connection.getAllActualVisitors();
            string body = "Se ha activado la alarma. Evacuar las visitas actuales: ";
            foreach (string name in names)
            {
                body += name + "\n";
            }
            email.sendEmail(SharedVars.config.username, SharedVars.config.contactoAlarma, body, null);

        }
        private void alarmButton_MouseLeave(object sender, EventArgs e)
        {
            this.alarmButton.BackgroundImage = global::VisitorRegister.Properties.Resources.grey_bell_outline_transparentpng_image_free_to_download;
        }

        private void alarmButton_MouseEnter(object sender, EventArgs e)
        {
            this.alarmButton.BackgroundImage = global::VisitorRegister.Properties.Resources.red_bell_outline_transparentpng_image_free_to_download;
        }

        private void loginButton_MouseEnter(object sender, EventArgs e)
        {
            this.loginButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
        }

        private void visitorButton_MouseEnter(object sender, EventArgs e)
        {
            this.visitorButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
        }

        private void visitorButton_MouseLeave(object sender, EventArgs e)
        {
            this.visitorButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
        }

        private void loginButton_MouseLeave(object sender, EventArgs e)
        {
            this.loginButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
        }

        private void configurationButton_MouseEnter(object sender, EventArgs e)
        {
            this.configurationButton.BackgroundImage = global::VisitorRegister.Properties.Resources._23_ROJO;
        }

        private void configurationButton_MouseLeave(object sender, EventArgs e)
        {
            this.configurationButton.BackgroundImage = global::VisitorRegister.Properties.Resources._231;
        }

        private void userControl11_Click(object sender, EventArgs e)
        {
            this.userControl11.textBox1.Text = "";
        }

        private void passwordControl1_Click(object sender, EventArgs e)
        {
            this.passwordControl1.textBox1.Text = "";
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            this.Refresh();
            /*string filename = SharedVars.config.contactPath;//"\\\\192.168.99.17\\datos\\INTERCAMBIO\\RegistroVisitas\\RequiredFiles\\Contactos_Ekide.xlsx";
            LoadContacts contacts = new LoadContacts(filename);
            contacts.LoadAllData();
            SharedVars.contacts = contacts;*/

            StartDatabaseConnection();
            //ConnectToExchangeService();
        }

    }
}
