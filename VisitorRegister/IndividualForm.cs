﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;
using VisitorRegister.src;

namespace VisitorRegister
{
    public partial class IndividualForm : System.Windows.Forms.Form
    {
        public delegate void ProcessDoneEvent(object sender, EventArgs e);

        //GUI.CardReaderControl cardReaderControl1;
        //GUI.NameControl nameControl1;
        //private Thread thread; 
        public IndividualForm()
        {
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();

            //this.nameControl1 = new GUI.NameControl();
            //this.nameControl1.Location = new System.Drawing.Point((1920/2), 200);
            //this.nameControl1.Name = "namecontrol1";
            //this.nameControl1.Size = new System.Drawing.Size(795, 436);
            //this.nameControl1.TabIndex = 3;
            //this.nameControl1.Visible = true;
            this.nameControl2.nextButton.Click += ChangeScreen;

            //this.Controls.Add(nameControl1);

            //this.cardReaderControl1 = new GUI.CardReaderControl();
            //this.cardReaderControl1.Location = new System.Drawing.Point((1920 / 2), 200);
            //this.cardReaderControl1.Name = "cardReaderControl1";
            //this.cardReaderControl1.Size = new System.Drawing.Size(795, 436);
            //this.cardReaderControl1.TabIndex = 3;

            //cardReaderControl1.bgWorker.RunWorkerCompleted += HideCardReader;
            //this.FormClosing += CardReaderForm_FormClosing;

            Update();
        }

        public IndividualForm(object[] data)
        {
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();


            this.nameControl2.nextButton.Click += ChangeScreen;
            LoadData(data);
            //LANGUAGES
            
            this.label1.Text = "USUARIO EXISTENTE";
            Update();
        }

        private void LoadData(object[] data)
        {
            nameControl2.dniTextBox.Text = data[0].ToString();
            nameControl2.nameTextBox.Text = data[1].ToString();
            nameControl2.empresaTextBox.Text = data[2].ToString();

            for (int i = 0; i < nameControl2.comboBox1.Items.Count; i++)
            {
                if (nameControl2.comboBox1.Items[i].Equals(data[3].ToString()))
                {
                    nameControl2.comboBox1.SelectedIndex = i;
                }
            }
        }

        //private void CardReaderForm_FormClosing(object sender, FormClosingEventArgs e)
        //{
        //    cardReaderControl1.closing = true;
        //    cardReaderControl1.state = 0;
        //}



        private void HideCardReader(string card)//object sender, RunWorkerCompletedEventArgs e)
        {
            //if (cardReaderControl1.closing)
            //{
            //    this.Close();
            //}
            if(CheckData(card))
            {

                SharedVars.actualVisitor.cardRegistered = card;//.ToString();
                SharedVars.actualVisitor.startDate = DateTime.Now;
                SharedVars.actualVisitorTotal.Add(SharedVars.actualVisitor);

                SharedVars.connection.insertVisitor(SharedVars.actualVisitor);

                SharedVars.connection.modifiedOn(true);
                Visitor.WriteToDisk();
                MetroFramework.MetroMessageBox.Show(this, "Welcome to Ekide, you have already warned your contact, remember to wipe your hands", "THANKS"); 
                this.Close();
            }
            

            //SharedVars.excel.exportDatabaseExcel();
            //Done done = new Done();
            //done.ShowDialog();

            //this.Close();
        }
        private bool CheckData(string v)
        {
            foreach (Visitor vis in SharedVars.actualVisitorTotal)
            {
                if (v == vis.cardRegistered)
                {
                    return false;
                }
            }
            return true;
        }
        public void ChangeScreen(object sender, EventArgs e)
        {
            if (nameControl2.validate)
            {
                CardReaderForm cRF = new CardReaderForm("LOGIN");
                cRF.ShowDialog();

                string card = cRF.cardRead;
                HideCardReader(card);
            }
            
        }
      
        private void backButton_MouseEnter(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._18;
        }

        private void backButton_MouseLeave(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
