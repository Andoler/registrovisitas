﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;
using VisitorRegister.src;

namespace VisitorRegister
{
    public partial class InfoData : System.Windows.Forms.Form
    {
        Visitor visitor;

        public InfoData(Visitor _visitor)
        {
            InitializeComponent();

            visitor = _visitor;
        }

        private void InfoData_Load(object sender, EventArgs e)
        {
            this.metroLabel11.Text = visitor.name;
            this.metroLabel12.Text = visitor.dni;
            this.metroLabel13.Text = visitor.company;
            this.metroLabel14.Text = visitor.startDate.ToShortTimeString();

            Contact contactVisitor = visitor.contact;

            this.nameContact.Text = contactVisitor.name;
            this.phoneContact.Text = contactVisitor.extension;
            this.emailContact.Text = contactVisitor.email;

        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            this.button1.BackgroundImage = global::VisitorRegister.Properties.Resources._16;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            this.button1.BackgroundImage = global::VisitorRegister.Properties.Resources._12_3;
        }

        private void backButton_MouseEnter(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._18;
        }

        private void backButton_MouseLeave(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
