﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisitorRegister
{
    public partial class MessageBoxForm : Form
    {
        public MessageBoxForm(String mensage)
        {
            InitializeComponent();
            this.label1.Text = mensage;
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
