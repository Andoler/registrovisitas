﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;
using VisitorRegister.Common;

namespace VisitorRegister.GUI
{
    public partial class CardReaderControl : UserControl
    {
        //Ultralight function
        [DllImport("umf32.DLL", EntryPoint = "fw_request_ultralt")]
        public static extern Int32 fw_request_ultralt(Int32 icdev, Byte _Mode);
        [DllImport("umf32.DLL", EntryPoint = "fw_anticall_ultralt")]
        public static extern Int32 fw_anticall_ultralt(Int32 icdev, ulong[] _Snr);
        [DllImport("umf32.DLL", EntryPoint = "fw_select_ultralt", CallingConvention =CallingConvention.Cdecl)]
        public static extern Int32 fw_select_ultralt(Int32 icdev, ulong _Snr);
        [DllImport("umf32.DLL", EntryPoint = "fw_write_ultralt")]
        public static extern Int32 fw_write_ultralt(Int32 icdev, Byte iPage, Byte[] wdata);
        [DllImport("umf32.DLL", EntryPoint = "fw_read_ultralt", CallingConvention = CallingConvention.Cdecl)]
        public static extern Int32 fw_read_ultralt(Int32 icdev, Byte iPage, Byte[] rdata);

        public System.Windows.Forms.Timer timerAnimation, timerReader;
        public System.Timers.Timer timer1, timer2;
        Point initialLocation;
        bool fordward, set;
        public int state = -1;
        public ulong[] cardData;
        public Thread thread;
        public bool finish = false ;
        public bool stop = false;
        public BackgroundWorker bgWorker;
        private delegate void SafeCallDelegate(Point p);
        PictureBox p;
        public bool closing;

        public CardReaderControl()
        {
            InitializeComponent();

            thread = new Thread(new ThreadStart(CheckReaderData));
            bgWorker = new BackgroundWorker();
            bgWorker.DoWork += BgWorker_DoWork;
            bgWorker.RunWorkerCompleted += BgWorker_RunWorkerCompleted;

            initialLocation = this.pictureBox1.Location;

            p = this.pictureBox1;
            fordward = true;
            set = false;
            cardData = new ulong[3];
            closing = false;

        }

        public void BgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ThreadState t = thread.ThreadState;
            thread.Abort();
        }

        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            thread.Start();
            MoveImage();
            
        }

        private void MoveImage()
        {
            while (state != 0)
            {
                if (pictureBox1.InvokeRequired)
                {
                    pictureBox1.Invoke(new MethodInvoker(
                    delegate ()
                    {
                        if (fordward)
                            this.pictureBox1.Location = new Point(this.pictureBox1.Location.X + 2, this.pictureBox1.Location.Y);
                        else
                            this.pictureBox1.Location = new Point(this.pictureBox1.Location.X - 5, this.pictureBox1.Location.Y);

                        if (this.pictureBox1.Location.X + this.pictureBox1.Width >= this.pictureBox2.Location.X)
                        {
                            fordward = false;
                        }

                        if (this.p.Location.X <= initialLocation.X)
                        {
                            fordward = true;
                        }
                        Update();
                    }));
                }
                else
                {
                    if (fordward)
                        this.pictureBox1.Location = new Point(this.pictureBox1.Location.X + 2, this.pictureBox1.Location.Y);
                    else
                        this.pictureBox1.Location = new Point(this.pictureBox1.Location.X - 5, this.pictureBox1.Location.Y);

                    if (this.pictureBox1.Location.X + this.pictureBox1.Width >= this.pictureBox2.Location.X)
                    {
                        fordward = false;
                    }

                    if (this.p.Location.X <= initialLocation.X)
                    {
                        fordward = true;
                    }
                    Update();
                }

                Thread.Sleep(10);
            }
        }


        public void Start()
        {
            bgWorker.RunWorkerAsync();
        }
        public void CheckReaderData()
        {
            if (closing)
                return;
            
                do
                {
                    try
                    {
                        state = fw_request_ultralt(SharedVars.readerID, 1);
                        state = fw_anticall_ultralt(SharedVars.readerID, cardData);
                        state = fw_select_ultralt(SharedVars.readerID, cardData[0]);
                    }catch(Exception EX)
                    {
                        string message = EX.Message;
                    }
                }
                while (state != 0);
        }

    }
}

