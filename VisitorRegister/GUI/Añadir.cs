﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisitorRegister
{
    public partial class Añadir : UserControl
    {
        public AñadirEmpleado employers;
        public Añadir()
        {
            InitializeComponent();

            employers = new AñadirEmpleado();
            employers.Visible = false;
            this.Controls.Add(employers);
        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {
            if(this.metrotextBox1.Text.Contains("Añadir..."))
                this.metrotextBox1.Text = "";
        }

    }
}
