﻿namespace VisitorRegister.GUI
{
    partial class LanguageSelector
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LanguageSelector));
            this.englishButton = new System.Windows.Forms.Button();
            this.spanishButton = new System.Windows.Forms.Button();
            this.euskeraButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // englishButton
            // 
            resources.ApplyResources(this.englishButton, "englishButton");
            this.englishButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.englishButton.FlatAppearance.BorderSize = 0;
            this.englishButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.englishButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            this.englishButton.Name = "englishButton";
            this.englishButton.UseVisualStyleBackColor = true;
            this.englishButton.Click += new System.EventHandler(this.englishButton_Click);
            this.englishButton.MouseEnter += new System.EventHandler(this.englishButton_MouseEnter);
            this.englishButton.MouseLeave += new System.EventHandler(this.englishButton_MouseLeave);
            // 
            // spanishButton
            // 
            resources.ApplyResources(this.spanishButton, "spanishButton");
            this.spanishButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.spanishButton.FlatAppearance.BorderSize = 0;
            this.spanishButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.spanishButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            this.spanishButton.Name = "spanishButton";
            this.spanishButton.UseVisualStyleBackColor = true;
            this.spanishButton.Click += new System.EventHandler(this.spanishButton_Click);
            this.spanishButton.MouseEnter += new System.EventHandler(this.spanishButton_MouseEnter);
            this.spanishButton.MouseLeave += new System.EventHandler(this.spanishButton_MouseLeave);
            // 
            // euskeraButton
            // 
            resources.ApplyResources(this.euskeraButton, "euskeraButton");
            this.euskeraButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.euskeraButton.FlatAppearance.BorderSize = 0;
            this.euskeraButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.euskeraButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            this.euskeraButton.Name = "euskeraButton";
            this.euskeraButton.UseVisualStyleBackColor = true;
            this.euskeraButton.Click += new System.EventHandler(this.euskeraButton_Click);
            this.euskeraButton.MouseEnter += new System.EventHandler(this.euskeraButton_MouseEnter);
            this.euskeraButton.MouseLeave += new System.EventHandler(this.euskeraButton_MouseLeave);
            // 
            // LanguageSelector
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.englishButton);
            this.Controls.Add(this.spanishButton);
            this.Controls.Add(this.euskeraButton);
            this.Name = "LanguageSelector";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button englishButton;
        private System.Windows.Forms.Button spanishButton;
        private System.Windows.Forms.Button euskeraButton;
    }
}
