﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Globalization;

namespace VisitorRegister.GUI
{
    public partial class LanguageSelector : UserControl
    {
        public LanguageSelector()
        {
            InitializeComponent();
        }

        private void euskeraButton_Click(object sender, EventArgs e)
        {
            ChangeLanguage("eu");
        }

        private void spanishButton_Click(object sender, EventArgs e)
        {
            ChangeLanguage("es-ES");
        }

        private void englishButton_Click(object sender, EventArgs e)
        {
            ChangeLanguage("en-US");

        }

        private static void ChangeLanguage(string lang)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(lang);
           
            foreach (Form frm in Application.OpenForms)
            {
                if(frm.ToString().Contains("VisitorRegister"))
                    localizeForm(frm);
            }
        }

        private static void localizeForm(Form frm)
        {
            var manager = new ComponentResourceManager(frm.GetType());

            manager.ApplyResources(frm, "$this");
            applyResources(manager, frm.Controls);
          
        }

        private static void applyResources(ComponentResourceManager manager, Control.ControlCollection ctls)
        {
            foreach (Control ctl in ctls)
            {
                Console.WriteLine(ctl.GetType().ToString());
                manager.ApplyResources(ctl, ctl.Name);
                applyResources(manager, ctl.Controls);
            }
        }

        private void englishButton_MouseEnter(object sender, EventArgs e)
        {
            this.englishButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
        }

        private void englishButton_MouseLeave(object sender, EventArgs e)
        {
            this.englishButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
        }

        private void spanishButton_MouseEnter(object sender, EventArgs e)
        {
            this.spanishButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
        }

        private void spanishButton_MouseLeave(object sender, EventArgs e)
        {
            this.spanishButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
        }

        private void euskeraButton_MouseEnter(object sender, EventArgs e)
        {
            this.euskeraButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(5)))), ((int)(((byte)(32)))));
        }

        private void euskeraButton_MouseLeave(object sender, EventArgs e)
        {
            this.euskeraButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
        }
    }
}
