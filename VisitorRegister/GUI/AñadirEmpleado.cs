﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisitorRegister
{
    public partial class AñadirEmpleado : UserControl
    {
        public string cardRead;
        public AñadirEmpleado()
        {
            InitializeComponent();
            cardRead = "";
        }

        private void metroLabel1_Click(object sender, EventArgs e)
        {
            if(this.textBox1.Text.Contains("Nombre") || this.textBox1.Text.Contains("Name"))
                this.textBox1.Text = "";

            if (this.textBox2.Text.Contains("DNI") || this.textBox2.Text.Contains("ID"))
                this.textBox2.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CardReaderForm cRF = new CardReaderForm("LOGIN");
            cRF.ShowDialog();
            cardRead = cRF.cardRead;
        }
    }
}
