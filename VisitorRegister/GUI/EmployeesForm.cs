﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;

namespace VisitorRegister.GUI
{
    public partial class EmployeesForm : System.Windows.Forms.Form
    {
        public List<Visitor> visitors { get; set; }
        public EmployeesForm()
        {
            InitializeComponent();
            visitors = new List<Visitor>();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if(this.metroPanel1.Controls.Count == 2)
            {
                AñadirEmpleado employee = new AñadirEmpleado();
                metroPanel1.Controls.Add(employee);
            }
            else
            {
                Point p = metroPanel1.Controls[metroPanel1.Controls.Count - 1].Location;
                AñadirEmpleado employee = new AñadirEmpleado();
                employee.Location = new Point(p.X, p.Y + metroPanel1.Controls[metroPanel1.Controls.Count - 1].Height);
                metroPanel1.Controls.Add(employee);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            List<Visitor> vs = new List<Visitor>();
                foreach (Control c in metroPanel1.Controls)
                {
                    if (c.GetType().ToString().Contains("AñadirEmpleado"))
                    {
                        AñadirEmpleado empleado = (AñadirEmpleado)c;
                        Visitor v = new Visitor(empleado.textBox1.Text, empleado.textBox2.Text, this.textBox1.Text,"" , DateTime.Now, new DateTime(), empleado.cardRead, true);
                        vs.Add(v);
                    }
                }

                visitors = vs;
            this.Close();
        }

        public void LoadData(Group g)
        {
            
            this.textBox1.Text = g.company;

            foreach(Visitor v in g.visitors)
            {
                AñadirEmpleado employee = new AñadirEmpleado();
                employee.cardRead = v.cardRegistered;
                employee.textBox1.Text = v.name;
                employee.textBox2.Text = v.dni;
                if (v.cardRegistered != "")
                {
                    employee.button1.Enabled = false;
                    employee.button1.Text = "";
                    employee.BackgroundImage = Image.FromFile(".\\Resources\\002-tick.png");
                    employee.BackgroundImageLayout = ImageLayout.Stretch;
                    //employee.button1.Visible = false;

                }

                if (this.metroPanel1.Controls.Count == 2)
                {
                    metroPanel1.Controls.Add(employee);
                }
                else
                {
                    Point p = metroPanel1.Controls[metroPanel1.Controls.Count - 1].Location;
                    employee.Location = new Point(p.X, p.Y + metroPanel1.Controls[metroPanel1.Controls.Count - 1].Height);
                    metroPanel1.Controls.Add(employee);
                }
            }

        }
    }
}