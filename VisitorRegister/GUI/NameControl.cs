﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.src;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using VisitorRegister.Common;

namespace VisitorRegister.GUI
{
    public partial class NameControl_PRE : UserControl
    {
        public bool validate = false;

        public List<string> _items;
        public NameControl_PRE()
        {
            InitializeComponent();

            var source = new AutoCompleteStringCollection();
            source.AddRange(new string[]
                            {
                        "Ekide",
                        "Vicomtech",
                        "Ekin",
                        "Fagor",
                        "Imesaza",
                        "Jovi",
                        "Infaimon"
                            });

            empresaTextBox.AutoCompleteCustomSource = source;
            _items = new List<string>();
            _items = SharedVars.contacts.getNames();
            foreach(string name in _items)
            {
                this.comboBox1.Items.Add(name);
                this.comboBox1.AutoCompleteCustomSource.Add(name);
            }

            this.Refresh();

        }

        private void NameControl_Load(object sender, EventArgs e)
        {
            //this.contactoComboBox.DataSource = SharedVars.contacts.getNames();

            //this.comboBoxText1._items = SharedVars.contacts.getNames();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            if(checkData())
            {
               
                try
                {
                    SharedVars.actualVisitor = new Visitor();
                    SharedVars.actualVisitor.company = this.empresaTextBox.Text;
                    SharedVars.actualVisitor.contact = SharedVars.contacts.getContactByName(this.comboBox1.SelectedItem.ToString());
                    SharedVars.actualVisitor.dni = this.dniTextBox.Text;
                    SharedVars.actualVisitor.name = this.nameTextBox.Text;
                    SharedVars.actualVisitor.stillInside = true;
                    sendEmail(SharedVars.actualVisitor);
                    validate = true;
                }
                catch
                {
                   MessageBox.Show("You did not enter contact correctly.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                
            }
            else
            {
                MessageBox.Show("You did not enter any parameters.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        public event EventHandler CloseButtonClicked;
        protected virtual void OnCloseButtonClicked(EventArgs e)
        {
            var handler = CloseButtonClicked;
            if (handler != null)
                handler(this, e);
        }
        private void CloseButton_Click(object sender, EventArgs e)
        {
            //While you can call `this.ParentForm.Close()` it's better to raise an event
            OnCloseButtonClicked(e);
        }
        private void sendEmail(Visitor v)
        {
            EmailService email = new EmailService();
            string body = "La visita: " + this.nameTextBox.Text + " de la empresa "+ v.company + " le espera en recepción. \n Registrado a las: " + DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString(); 
            email.sendEmail(SharedVars.config.username, v.contact.email, body, v);
            

        }

        private bool checkData()
        {
            bool check = false;
            try
            {
                if (this.comboBox1.SelectedItem.ToString() != "")
                {
                    if (_items.Contains(this.comboBox1.SelectedItem.ToString()))
                    {
                        check = true;
                    }
                    else
                    {
                        check = false;
                    }
                }
          
            }
            catch
            {
                check = false;
            }
            return check;
        }

        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            this.comboBox1.DroppedDown = true;
        }

        private void nextButton_MouseEnter(object sender, EventArgs e)
        {
            this.nextButton.BackgroundImage = global::VisitorRegister.Properties.Resources._16;
        }

        private void nextButton_MouseLeave(object sender, EventArgs e)
        {
            this.nextButton.BackgroundImage = global::VisitorRegister.Properties.Resources._12_3;
        }

        private void dniTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (dniTextBox.Text.Length < 7)
            {
                e.Cancel = true;
                dniTextBox.Select(0, dniTextBox.Text.Length);
                errorProvider1.SetError(dniTextBox, "No has inserted all digits");
            }
        }

        private void dniTextBox_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(dniTextBox, "");
        }

        private void nameTextBox_Validating(object sender, CancelEventArgs e)
        {

            if (nameTextBox.Text.Length <= 5)
            {
                e.Cancel = true;
                nameTextBox.Select(0, nameTextBox.Text.Length);
                errorProvider1.SetError(nameTextBox, "You must enter your name and surname");
            }
            if(!nameTextBox.Text.Contains(" "))
            {
                e.Cancel = true;
                nameTextBox.Select(0, nameTextBox.Text.Length);
                errorProvider1.SetError(nameTextBox, "You must enter your name and surname");
            }
            foreach (char caracter in nameTextBox.Text)
            {
                if (char.IsDigit(caracter))
                {
                    e.Cancel = true;
                    errorProvider1.SetError(nameTextBox, "There must be no numbers");
                }
            }
        }
        private void nameTextBox_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(nameTextBox, "");
        }
        

        private void empresaTextBox_Validating(object sender, CancelEventArgs e)
        {
            if (empresaTextBox.Text.Length <=2 )
            {
                e.Cancel = true;
                empresaTextBox.Select(0, empresaTextBox.Text.Length);
                errorProvider1.SetError(empresaTextBox, "You must enter the name of the company");
            }
            
        }
        private void empresaTextBox_Validated(object sender, EventArgs e)
        {
            errorProvider1.SetError(empresaTextBox, "");
        }
    }
}
