﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;

namespace VisitorRegister.GUI
{
    public partial class GroupUser : UserControl
    {
        public Visitor visitor;
        public string textboxAssoc { get; set; }

        public GroupUser()
        {
            InitializeComponent();
            visitor = new Visitor();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "")
            {
                CardReaderForm cR = new CardReaderForm("LOGIN");
                cR.ShowDialog();
                string card = cR.cardRead;

                visitor.name = textBox3.Text;
                visitor.dni = textBox4.Text;
                visitor.startDate = DateTime.Now;
                visitor.cardRegistered = card;
                visitor.company = textBox2.Text;
                //Añadir a actual
            }
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            this.button1.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            this.button1.ForeColor = System.Drawing.Color.FromArgb(227, 227, 226);
        }

        private void GroupUser_Load(object sender, EventArgs e)
        {

        }
    }
}
