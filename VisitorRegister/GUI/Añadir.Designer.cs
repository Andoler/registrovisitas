﻿namespace VisitorRegister
{
    partial class Añadir
    {
        /// <summary> 
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de componentes

        /// <summary> 
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metrotextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.SuspendLayout();
            // 
            // metroButton1
            // 
            this.metroButton1.BackColor = System.Drawing.Color.DarkTurquoise;
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton1.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.metroButton1.Location = new System.Drawing.Point(154, 3);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(35, 32);
            this.metroButton1.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroButton1.TabIndex = 4;
            this.metroButton1.Text = "+";
            this.metroButton1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroButton1.UseCustomBackColor = true;
            this.metroButton1.UseSelectable = true;
            // 
            // metrotextBox1
            // 
            this.metrotextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            // 
            // 
            // 
            this.metrotextBox1.CustomButton.Image = null;
            this.metrotextBox1.CustomButton.Location = new System.Drawing.Point(102, 2);
            this.metrotextBox1.CustomButton.Name = "";
            this.metrotextBox1.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.metrotextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metrotextBox1.CustomButton.TabIndex = 1;
            this.metrotextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metrotextBox1.CustomButton.UseSelectable = true;
            this.metrotextBox1.CustomButton.Visible = false;
            this.metrotextBox1.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.metrotextBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.metrotextBox1.Lines = new string[] {
        "Añadir...            "};
            this.metrotextBox1.Location = new System.Drawing.Point(5, 3);
            this.metrotextBox1.MaxLength = 32767;
            this.metrotextBox1.Name = "metrotextBox1";
            this.metrotextBox1.PasswordChar = '\0';
            this.metrotextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metrotextBox1.SelectedText = "";
            this.metrotextBox1.SelectionLength = 0;
            this.metrotextBox1.SelectionStart = 0;
            this.metrotextBox1.ShortcutsEnabled = true;
            this.metrotextBox1.Size = new System.Drawing.Size(132, 32);
            this.metrotextBox1.TabIndex = 3;
            this.metrotextBox1.Text = "Añadir...            ";
            this.metrotextBox1.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.metrotextBox1.UseCustomBackColor = true;
            this.metrotextBox1.UseCustomForeColor = true;
            this.metrotextBox1.UseSelectable = true;
            this.metrotextBox1.WaterMarkColor = System.Drawing.Color.Black;
            this.metrotextBox1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.metrotextBox1.Click += new System.EventHandler(this.metroLabel1_Click);
            // 
            // Añadir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.metrotextBox1);
            this.Name = "Añadir";
            this.Size = new System.Drawing.Size(195, 42);
            this.ResumeLayout(false);

        }

        #endregion

        public MetroFramework.Controls.MetroButton metroButton1;
        public MetroFramework.Controls.MetroTextBox metrotextBox1;
    }
}
