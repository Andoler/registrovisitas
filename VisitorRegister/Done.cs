﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VisitorRegister
{
    public partial class Done : Form
    {
        public Done()
        {
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
            bunifuFormFadeTransition1.TransitionEnd += BunifuFormFadeTransition1_TransitionEnd;
            this.CenterToScreen();
        }

        private void BunifuFormFadeTransition1_TransitionEnd(object sender, EventArgs e)
        {
            Thread.Sleep(2);
            this.Close();
        }

        private void Done_Load(object sender, EventArgs e)
        {
            bunifuFormFadeTransition1.ShowAsyc(this);
        }
    }
}
