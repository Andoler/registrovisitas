﻿namespace VisitorRegister
{
    partial class VisitorInit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VisitorInit));
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.grupalButton = new System.Windows.Forms.Button();
            this.individualButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
            resources.ApplyResources(this.button1, "button1");
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // backButton
            // 
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
            resources.ApplyResources(this.backButton, "backButton");
            this.backButton.FlatAppearance.BorderSize = 0;
            this.backButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.backButton.Name = "backButton";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            this.backButton.MouseEnter += new System.EventHandler(this.backButton_MouseEnter);
            this.backButton.MouseLeave += new System.EventHandler(this.backButton_MouseLeave);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::VisitorRegister.Properties.Resources._3_1;
            resources.ApplyResources(this.pictureBox3, "pictureBox3");
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.TabStop = false;
            // 
            // grupalButton
            // 
            this.grupalButton.BackgroundImage = global::VisitorRegister.Properties.Resources._8;
            resources.ApplyResources(this.grupalButton, "grupalButton");
            this.grupalButton.FlatAppearance.BorderSize = 0;
            this.grupalButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
            this.grupalButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            this.grupalButton.Name = "grupalButton";
            this.grupalButton.UseVisualStyleBackColor = true;
            this.grupalButton.Click += new System.EventHandler(this.grupalButton_Click);
            this.grupalButton.MouseEnter += new System.EventHandler(this.grupalButton_MouseEnter);
            this.grupalButton.MouseLeave += new System.EventHandler(this.grupalButton_MouseLeave);
            // 
            // individualButton
            // 
            this.individualButton.BackgroundImage = global::VisitorRegister.Properties.Resources._7;
            resources.ApplyResources(this.individualButton, "individualButton");
            this.individualButton.FlatAppearance.BorderSize = 0;
            this.individualButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
            this.individualButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            this.individualButton.Name = "individualButton";
            this.individualButton.UseVisualStyleBackColor = true;
            this.individualButton.Click += new System.EventHandler(this.individualButton_Click);
            this.individualButton.MouseEnter += new System.EventHandler(this.individualButton_MouseEnter);
            this.individualButton.MouseLeave += new System.EventHandler(this.individualButton_MouseLeave);
            // 
            // VisitorInit
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.grupalButton);
            this.Controls.Add(this.individualButton);
            this.Name = "VisitorInit";
            this.Load += new System.EventHandler(this.VisitorInit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button individualButton;
        private System.Windows.Forms.Button grupalButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button backButton;
    }
}