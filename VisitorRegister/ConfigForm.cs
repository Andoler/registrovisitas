﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;

namespace VisitorRegister
{
    public partial class ConfigForm : System.Windows.Forms.Form
    {
        public bool isCancelled;
        public ConfigForm()
        {
            InitializeComponent();

            isCancelled = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox2.SelectedItem.ToString().Equals("Gmail"))
            {
                label7.Enabled = false;
                textBox7.Enabled = false;
            }
            else
            {
                if (label7.Enabled == false)
                    label7.Enabled = true;
                if (textBox7.Enabled == false)
                    textBox7.Enabled = true; 
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!readAllData())
            {
                MetroFramework.MetroMessageBox.Show(this, "Debes rellenar todos los campos.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return; 
            }

            saveConfigFile();
            isCancelled = false;
            this.Close();
        }

        private void saveConfigFile()
        {
            SharedVars.config.WriteToDisk(Environment.CurrentDirectory + "\\ConfigOpt.xml");
        }

        private bool readAllData()
        {
            if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox8.Text == "" || textBox6.Text == "" || textBox7.Text == "")
                return false;

            SharedVars.config.serverConnection = "Server="+textBox1.Text+";Database="+ textBox2.Text + ";User Id="+ textBox3.Text + ";Password="+ textBox4.Text;

            SharedVars.config.emailType = comboBox1.SelectedItem.ToString() ;
            SharedVars.config.username = textBox5.Text;
            SharedVars.config.password = textBox6.Text;

            SharedVars.config.domain = "";
            if (SharedVars.config.emailType.Equals("Outlook"))
                SharedVars.config.domain = textBox7.Text;

            SharedVars.config.contactPath = metroLink1.Text;
            SharedVars.config.Location = comboBox2.SelectedItem.ToString();

            SharedVars.config.contactoAlarma = textBox8.Text;

            return true;
        }

        private void ConfigForm_Load(object sender, EventArgs e)
        {
            string[] parameters = SharedVars.config.serverConnection.Split(';');

            textBox1.Text = parameters[0].Substring("Server=".Length, parameters[0].Length - "Server=".Length);
            textBox2.Text = parameters[1].Substring("Database=".Length, parameters[1].Length - "Database=".Length);
            textBox3.Text = parameters[2].Substring("User Id=".Length, parameters[2].Length - "User Id=".Length);
            textBox4.Text = parameters[3].Substring("Password=".Length, parameters[3].Length - "Password=".Length);

            if (SharedVars.config.emailType.Equals("Gmail"))
                comboBox1.SelectedIndex = 1;
            else
                comboBox1.SelectedIndex = 0;


            textBox5.Text = SharedVars.config.username;
            textBox6.Text = SharedVars.config.password;

            textBox7.Text = SharedVars.config.domain;

            metroLink1.Text = SharedVars.config.contactPath;

            if (SharedVars.config.Location.Equals("Arrasate"))
                comboBox2.SelectedIndex = 0;
            else if (SharedVars.config.Location.Equals("Zamudio"))
                comboBox2.SelectedIndex = 1;
            else
                comboBox2.SelectedIndex = 2;

            textBox8.Text = SharedVars.config.contactoAlarma;


        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.DefaultExt = (".xlsx");
            openFile.InitialDirectory = SharedVars.config.contactPath.Substring(0, SharedVars.config.contactPath.LastIndexOf("\\"));
            openFile.FileName = SharedVars.config.contactPath;
            //openFile.OpenFile();
            DialogResult dR = openFile.ShowDialog();

            if (dR.ToString() == "OK")
            {
                SharedVars.config.contactPath = openFile.FileName;
                metroLink1.Text = SharedVars.config.contactPath;
            }
        }

        private void metroLink1_Click(object sender, EventArgs e)
        {
            string path = SharedVars.config.contactPath.Substring(0, SharedVars.config.contactPath.LastIndexOf("\\"));
            Process.Start(path);
        }

        private void metroButton3_MouseDown(object sender, MouseEventArgs e)
        {
            textBox6.UseSystemPasswordChar = false;
        }

        private void metroButton2_MouseDown(object sender, MouseEventArgs e)
        {
            textBox4.UseSystemPasswordChar = false;
        }

        private void metroButton2_MouseUp(object sender, MouseEventArgs e)
        {
            textBox4.UseSystemPasswordChar = true;
        }

        private void metroButton3_MouseUp(object sender, MouseEventArgs e)
        {
            textBox6.UseSystemPasswordChar = true;
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            this.button1.BackgroundImage = global::VisitorRegister.Properties.Resources._17;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            this.button1.BackgroundImage = global::VisitorRegister.Properties.Resources._14;
        }

        private void backButton_MouseEnter(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._18;
        }

        private void backButton_MouseLeave(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            isCancelled = true;
            this.Close();
        }

       
    }
}
