﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;
using VisitorRegister.GUI;
using VisitorRegister.src;

namespace VisitorRegister
{
    public partial class GroupForm : System.Windows.Forms.Form
    {
        List<string> _items;
        int numTextBoxes;
        public GroupForm()
        {
            numTextBoxes = 1;
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
           // groups = new List<Group>();

            _items = new List<string>();
            _items = SharedVars.contacts.getNames();
            foreach (string name in _items)
            {
                this.comboBox1.Items.Add(name);
                this.comboBox1.AutoCompleteCustomSource.Add(name);
            }
            groupUser1.textboxAssoc = "textBox1";

            Update();
        }
        private void comboBox1_KeyDown(object sender, KeyEventArgs e)
        {
            this.comboBox1.DroppedDown = true;
        }
        

        private void button2_Click(object sender, EventArgs e)
        {
            if((comboBox1.SelectedItem == null)&&(!_items.Contains(this.comboBox1.SelectedItem.ToString())))
            {
                MessageBox.Show("Debe seleccionar primero una persona de contacto. ", "INFO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            } 

            Contact general = new Contact();
            foreach (Control c in metroPanel1.Controls)
            {
                if (c.GetType().Equals(groupUser1.GetType()))
                {
                    GroupUser gU = (GroupUser)c;

                    //Contact contact = SharedVars.contacts.getContactByName(comboBox1.SelectedItem.ToString());
                    general = SharedVars.contacts.getContactByName(comboBox1.SelectedItem.ToString());
                    if (!CheckData(gU.visitor.cardRegistered))
                    {
                        continue;
                    }
                    gU.visitor.contact = general;//contact;
                    SharedVars.actualVisitorTotal.Add(gU.visitor);
                    SharedVars.connection.insertVisitor(gU.visitor);
                }
            }

            SharedVars.connection.modifiedOn(true);
            Visitor.WriteToDisk();
            sendEmail(general);

            this.Close();
        }
        private bool CheckData(string v)
        {
            foreach (Visitor vis in SharedVars.actualVisitorTotal)
            {
                if (v == vis.cardRegistered)
                {
                    return false;
                }
            }
            return true;
        }
        private void sendEmail(Contact contact)
        {
            EmailService email = new EmailService();
            string body = "La visita en grupo le espera en recepción. \n Registrado a las: " + DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString();
            email.sendEmail(SharedVars.config.username, contact.email, body, null);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //other company
            numTextBoxes++;
            TextBox textBox = new TextBox();
            textBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
            textBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            textBox.Location = this.button5.Location;
            textBox.TextChanged += textBox1_TextChanged;
            textBox.Font = textBox1.Font;
            textBox.Size = textBox1.Size;
            textBox.CharacterCasing = textBox1.CharacterCasing;
            textBox.Name = "textBox" + numTextBoxes.ToString();

            GroupUser gU = new GroupUser();
            gU.Location = new Point(groupUser1.Location.X, textBox.Location.Y + textBox.Height + 5);
            gU.textboxAssoc = textBox.Name;

            metroPanel1.Controls.Add(textBox);
            metroPanel1.Controls.Add(gU);

            this.button4.Location = new Point(this.button4.Location.X, this.button4.Location.Y + gU.Height + textBox.Height );
            this.button5.Location = new Point(this.button5.Location.X, this.button5.Location.Y + gU.Height + textBox.Height );


            VScrollProperties vp = metroPanel1.VerticalScroll;
            if (vp.Value != 0)
                vp.Value = vp.Value + gU.Height + + textBox.Height +  button4.Height + 7;
            metroPanel1.Refresh();

            Update();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            GroupUser gU = new GroupUser();
            gU.Location = new Point(groupUser1.Location.X, button4.Location.Y);
            gU.textboxAssoc = "textBox" + numTextBoxes.ToString();
            foreach(Control c in metroPanel1.Controls)
            {
                if(c.GetType().Equals(textBox1.GetType()))
                {
                    TextBox text1 = (TextBox)c;
                    if (c.Name == gU.textboxAssoc)
                    {
                        gU.textBox2.Text = c.Text;
                    }
                }
            }
            

            this.button4.Location = new Point(this.button4.Location.X, this.button4.Location.Y + gU.Height + 7);
            this.button5.Location = new Point(this.button5.Location.X, this.button5.Location.Y + gU.Height + 7);

            metroPanel1.Controls.Add(gU);
            VScrollProperties vp = metroPanel1.VerticalScroll;
            if(vp.Value != 0)
                vp.Value = vp.Value + gU.Height + 7;
            metroPanel1.Refresh();
            Update();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            foreach(Control c in metroPanel1.Controls)
            {
                if (c.GetType().Equals(groupUser1.GetType()))
                {
                    GroupUser gU = (GroupUser)c;
                    TextBox calling = (TextBox)sender;
                    if (gU.textboxAssoc == calling.Name)
                    {
                        gU.textBox2.Text = calling.Text; 
                    }
                }
            }
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            this.button4.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            this.button4.ForeColor = System.Drawing.Color.FromArgb(227, 227, 226);
        }

        private void button5_MouseEnter(object sender, EventArgs e)
        {
            this.button5.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
        }

        private void button5_MouseLeave(object sender, EventArgs e)
        {
            this.button5.ForeColor = System.Drawing.Color.FromArgb(227, 227, 226);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            this.button3.BackgroundImage = global::VisitorRegister.Properties.Resources._18;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            this.button3.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            this.button2.BackgroundImage = global::VisitorRegister.Properties.Resources._17;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            this.button2.BackgroundImage = global::VisitorRegister.Properties.Resources._14;
        }
    }
}
