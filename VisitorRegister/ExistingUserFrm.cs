﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;

namespace VisitorRegister
{
    public partial class ExistingUserFrm : Form
    {
        public ExistingUserFrm()
        {
            InitializeComponent();
        }
        private void Parent_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
            this.Close();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void backButton_MouseEnter(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._18;
        }

        private void backButton_MouseLeave(object sender, EventArgs e)
        {
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            this.button1.BackgroundImage = global::VisitorRegister.Properties.Resources._16;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            this.button1.BackgroundImage = global::VisitorRegister.Properties.Resources._12_3;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text == "")
            {
                MetroFramework.MetroMessageBox.Show(this, "ID Missing");
                return;
            }
            object[] data = CheckDNI(textBox1.Text);

            if (data == null)
            {
                MetroFramework.MetroMessageBox.Show(this, "ID not found");
                return;
            }

            IndividualForm individualForm = new IndividualForm(data);
            individualForm.FormClosed += Parent_FormClosed;
            individualForm.Show();
            this.Hide();
        }

        private object[] CheckDNI(string text)
        {
            //Check DNI
            try
            {
                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Visitas where DNI Like '" + textBox1.Text + "'", SharedVars.connection.connection);

                SqlDataReader reader = sqlCommand.ExecuteReader();

                while (reader.Read())
                {
                    object[] data = new object[reader.FieldCount];
                    reader.GetValues(data);

                    if (reader.FieldCount > 0)
                    {
                        return data;
                        //nameControl1.dniTextBox.Text = data[0].ToString();
                        //nameControl1.nameTextBox.Text = data[1].ToString();
                        //nameControl1.empresaTextBox.Text = data[2].ToString();

                        //for (int i = 0; i < nameControl1.comboBox1.Items.Count; i++)
                        //{
                        //    if (nameControl1.comboBox1.Items[i].Equals(data[3].ToString()))
                        //    {
                        //        nameControl1.comboBox1.SelectedIndex = i;
                        //    }
                        //}
                    }
                }
                return null;
                //reader.Close();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
