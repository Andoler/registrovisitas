﻿namespace VisitorRegister
{
    partial class chekinlogout
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(chekinlogout));
            this.languageSelector1 = new VisitorRegister.GUI.LanguageSelector();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.checkoutButton = new System.Windows.Forms.Button();
            this.infoButton = new System.Windows.Forms.Button();
            this.checkinButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // languageSelector1
            // 
            resources.ApplyResources(this.languageSelector1, "languageSelector1");
            this.languageSelector1.Name = "languageSelector1";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::VisitorRegister.Properties.Resources._3_1;
            resources.ApplyResources(this.pictureBox3, "pictureBox3");
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.TabStop = false;
            // 
            // checkoutButton
            // 
            this.checkoutButton.BackgroundImage = global::VisitorRegister.Properties.Resources._2;
            resources.ApplyResources(this.checkoutButton, "checkoutButton");
            this.checkoutButton.FlatAppearance.BorderSize = 0;
            this.checkoutButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
            this.checkoutButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            this.checkoutButton.Name = "checkoutButton";
            this.checkoutButton.UseVisualStyleBackColor = true;
            this.checkoutButton.Click += new System.EventHandler(this.checkoutButton_Click);
            this.checkoutButton.MouseEnter += new System.EventHandler(this.checkoutButton_MouseEnter);
            this.checkoutButton.MouseLeave += new System.EventHandler(this.checkoutButton_MouseLeave);
            // infoButton_MouseEnter
            // infoButton
            // 
            this.infoButton.BackgroundImage = global::VisitorRegister.Properties.Resources._4_1;
            resources.ApplyResources(this.infoButton, "infoButton");
            this.infoButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.infoButton.FlatAppearance.BorderSize = 0;
            this.infoButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
            this.infoButton.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.infoButton.Name = "infoButton";
            this.infoButton.UseVisualStyleBackColor = true;
            this.infoButton.Click += new System.EventHandler(this.infoButton_Click);
            this.infoButton.MouseEnter += new System.EventHandler(this.infoButton_MouseEnter);
            this.infoButton.MouseLeave += new System.EventHandler(this.infoButton_MouseLeave);
            // 
            // checkinButton
            // 
            this.checkinButton.BackgroundImage = global::VisitorRegister.Properties.Resources._1;
            resources.ApplyResources(this.checkinButton, "checkinButton");
            this.checkinButton.FlatAppearance.BorderSize = 0;
            this.checkinButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
            this.checkinButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(59)))));
            this.checkinButton.Name = "checkinButton";
            this.checkinButton.UseVisualStyleBackColor = true;
            this.checkinButton.Click += new System.EventHandler(this.checkinButton_Click);
            this.checkinButton.MouseEnter += new System.EventHandler(this.checkinButton_MouseEnter);
            this.checkinButton.MouseLeave += new System.EventHandler(this.checkinButton_MouseLeave);
            // 
            // backButton
            // 
            this.backButton.BackgroundImage = global::VisitorRegister.Properties.Resources._5_1;
            resources.ApplyResources(this.backButton, "backButton");
            this.backButton.FlatAppearance.BorderSize = 0;
            this.backButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.backButton.Name = "backButton";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            this.backButton.MouseEnter += new System.EventHandler(this.backButton_MouseEnter);
            this.backButton.MouseLeave += new System.EventHandler(this.backButton_MouseLeave);
            // 
            // chekinlogout
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(227)))), ((int)(((byte)(227)))), ((int)(((byte)(226)))));
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.languageSelector1);
            this.Controls.Add(this.checkoutButton);
            this.Controls.Add(this.infoButton);
            this.Controls.Add(this.checkinButton);
            this.Name = "chekinlogout";
            this.Load += new System.EventHandler(this.chekinlogout_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button checkinButton;
        private System.Windows.Forms.Button infoButton;
        private System.Windows.Forms.Button checkoutButton;
        private GUI.LanguageSelector languageSelector1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Button backButton;
    }
}