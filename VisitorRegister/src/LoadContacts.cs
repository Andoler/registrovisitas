﻿using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VisitorRegister.src
{
    public class LoadContacts
    {
        private string[] letters = { "A", "B", "C"};
        //private string filename;
        public List<Contact> contacts { get; set; }

        public LoadContacts()
        {
            contacts = new List<Contact>();
        }
       /* public LoadContacts(string _filename)
        {
            contacts = new List<Contact>();
            filename = _filename;
        }*/
        public List<string> getNames()
        {
            List<string> names = new List<string>() ;
            if (contacts != null)
            {
                foreach (Contact c in contacts)
                {
                    names.Add(c.name);
                }
            }
            return names;
        }
        /*public void LoadAllData()
        {
            if (File.Exists(filename))
            {
                SpreadsheetLight.SLDocument sl = new SpreadsheetLight.SLDocument(filename);
                SLWorksheetStatistics stats = sl.GetWorksheetStatistics();
                int endColumnIndex = stats.EndColumnIndex;
                int endRowIndex = stats.EndRowIndex;

                for(int i = 2; i< endRowIndex; i++)
                {
                    if (sl.GetCellValueAsString(i, 4) == null || sl.GetCellValueAsString(i, 4) == "")
                        continue; 

                    Contact c = new Contact();
                    c.email = sl.GetCellValueAsString(i, 4);
                    c.extension = sl.GetCellValueAsString(i, 7);
                    c.name = sl.GetCellValueAsString(i,2) + " " + sl.GetCellValueAsString(i, 3);
                    c.phone = sl.GetCellValueAsString(i, 5);
                    contacts.Add(c);              
                }
            }
        }*/

        public Contact getContactByName(string name)
        {
            if(contacts!= null)
            {
                foreach (Contact c in contacts)
                {
                    if (name.Equals(c.name) || c.email.Contains(name))
                    {
                        return c;
                    }
                }
            }

            return null;
        }
    }
}
