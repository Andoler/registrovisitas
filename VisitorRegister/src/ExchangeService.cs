﻿using AddressListSample;
using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VisitorRegister.Common;

namespace VisitorRegister.src
{
    public class Exchange
    {
        public ExchangeService service;
        public Exchange()
        {

        }
        public bool checkCredentials()
        {
            bool isValid = false;
            try
            {
                using (PrincipalContext pc = new PrincipalContext(ContextType.Domain, SharedVars.config.domain))
                {
                    Console.WriteLine("validate the credentials");
                    // validate the credentials
                    isValid = pc.ValidateCredentials(SharedVars.config.username, SharedVars.config.password); //TODO CAMBIAR

                }
                return isValid;
            }
            catch (Exception Ex)
            {
                Console.WriteLine("I don´t know what happens "+Ex.Message);
                return false;
            }
        }
        public List<VisitorRegister.src.Contact> retrieveContacts()
        {
            List<Microsoft.Exchange.WebServices.Data.Contact> contactos = new List<Microsoft.Exchange.WebServices.Data.Contact>();
            List<VisitorRegister.src.Contact> contacts = new List<Contact>();


            using (var context = new PrincipalContext(ContextType.Domain, SharedVars.config.domain))
            {
                using (var searcher = new PrincipalSearcher(new UserPrincipal(context)))
                {
                    foreach (var result in searcher.FindAll())
                    {
                        VisitorRegister.src.Contact contacto = new Contact();

                        DirectoryEntry de = result.GetUnderlyingObject() as DirectoryEntry;
                        if (de.Properties["department"].Value == null || de.Properties["mail"].Value == null)
                            continue;

                        string department = de.Properties["department"].Value.ToString();
                        string mail = de.Properties["mail"].Value.ToString();
                        if (department == "" || mail == "")
                            continue;

                        contacto.email = de.Properties["mail"].Value.ToString();
                        contacto.name = de.Properties["displayName"].Value.ToString();

                        if (de.Properties["telephoneNumber"].Value == null)
                            contacto.extension = "";
                        else
                            contacto.extension = de.Properties["telephoneNumber"].Value.ToString();

                        if (de.Properties["mobile"].Value == null)
                            contacto.phone = "";
                        else
                            contacto.phone = de.Properties["mobile"].Value.ToString();

                        contacts.Add(contacto);
                    }
                }
            }
            return contacts;

        }

        /*public void sendEmail()
        {
            service = new ExchangeService();
            service.AutodiscoverUrl(SharedVars.config.username);
            NameResolutionCollection searchResult = service.ResolveName("Leire Varona", ResolveNameSearchLocation.DirectoryThenContacts, true);
            foreach (NameResolution resolution in searchResult)
            {
                if (resolution.Contact == null)
                    continue;

                EmailMessage email = new EmailMessage(service);
                email.ToRecipients.Add(resolution.Mailbox.Address);
                email.Body = "La visita en grupo le espera en recepción. \n Registrado a las: " + DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString();
                email.Subject = "Recepcción Visitas";
                email.SendAndSaveCopy();

            }
        }*/

    }
}

//ekide@ekide.es a modificar en configuracion.
//SQL SERVER: ser12ekide
