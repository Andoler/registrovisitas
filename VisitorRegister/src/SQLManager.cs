﻿using SpreadsheetLight;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using VisitorRegister.Common;

namespace VisitorRegister.src
{
    public class SQLManager
    {
        public SqlConnection connection;

        public SQLManager()
        {
            connection = new SqlConnection(SharedVars.config.serverConnection);

            try
            {
                if ((connection != null) && (connection.State == System.Data.ConnectionState.Closed))
                {
                    connection.Open();
                    Console.WriteLine("Connection " + connection.State);
                }

                //SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Visitas", connection);
                //SqlDataReader reader = sqlCommand.ExecuteReader();


                //while (reader.Read())
                //{
                //    object[] data = new object[reader.FieldCount];
                //    reader.GetValues(data);
                //}
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                System.Windows.Forms.MessageBox.Show("fail to connection to "+SharedVars.config.serverConnection, "Error" , MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public bool insertVisitor(Visitor v)
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                String sql = "";
                sql = "Insert into Visitas(DNI,Name, Contact, Company, StartDate) values ('" + v.dni + "', '" + v.name + "', '" + v.contact.name + "', '" + v.company + "', '" + v.startDate + "');";
                SqlCommand command = new SqlCommand(sql, connection);
                adapter.InsertCommand = new SqlCommand(sql, connection);
                adapter.InsertCommand.ExecuteNonQuery();
                
                return true;
            }
            catch(Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show("Fallo en el insertVisitor.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;

            }
        }

        public bool deleteVisitor(Visitor v)
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                String sql = "";
                sql = "Delete from Visitas where DNI Like '" + v.dni + "' AND StartDate = @startDateParam";
                SqlCommand command = new SqlCommand(sql, connection); 
                command.Parameters.Add("@startDateParam", System.Data.SqlDbType.DateTime).Value = v.startDate.ToString("yyyy-MM-dd HH:mm:ss");
                command.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show("Fallo en el deleteVisitor.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }
        public bool getVisitor(string param, string value)
        {

            return false;
        }

        public bool modifiedOn(bool on)
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                String sql = "";

                if (on)
                    sql = "Update Modified SET modified = '1'";
                else
                    sql = "Update Modified SET modified = '0'";

                SqlCommand command = new SqlCommand(sql, connection);
                adapter.UpdateCommand = new SqlCommand(sql, connection);
                adapter.UpdateCommand.ExecuteNonQuery();
                return true;
            }
            catch(Exception ex)
            {

                //System.Windows.Forms.MessageBox.Show("Fallo en el modifiedOn.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false; 
            }
        }
      

        public bool updateVisitor(Visitor v, string param)
        {
            try
            {
                var value = getValue(param, v);

                //SqlDataAdapter adapter = new SqlDataAdapter();
                String sql = "";
                if (param != "FinishDate")
                {
                    sql = "Update Visitas SET " + param + " = '" + value + "' where DNI Like '" + v.dni + "' AND StartDate = @startDateParam";
                }
                else
                    sql = "Update Visitas SET " + param + " = @endDateParam where DNI Like '" + v.dni + "' AND StartDate = @startDateParam";
                SqlCommand command = new SqlCommand(sql, connection);
                command = new SqlCommand(sql, connection);
                if(param == "FinishDate")
                {
                    DateTime date = Convert.ToDateTime(value);
                    command.Parameters.Add("@endDateParam", System.Data.SqlDbType.DateTime).Value = date.ToString("yyyy-MM-dd HH:mm:ss");
                }
                    

                command.Parameters.Add("@startDateParam", System.Data.SqlDbType.DateTime).Value = v.startDate.ToString("yyyy-MM-dd HH:mm:ss");
                command.ExecuteNonQuery();

                return true;
            }
            catch (Exception ex)
            {

                //System.Windows.Forms.MessageBox.Show("Fallo en el updateVisitor.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        private object getValue(string param, Visitor v)
        {
            var value = new object();

            switch(param)
            {
                case "Name":
                    value = v.name;
                    break;
                case "DNI":
                    value = v.dni;
                    break;
                case "Company":
                    value = v.company;
                    break;
                case "Contact":
                    value = v.contact.name;
                    break;
                case "StartDate":
                    value = v.startDate.ToString();
                    break;
                case "FinishDate":
                    value = v.finishDate.ToString();
                    break;
            }

            return value;
        }

        public bool exportExcel(DateTime start, DateTime end, string outputFile)
        {
            try
            {
                List<Visitor> visitors = new List<Visitor>();
                if (connection.State != System.Data.ConnectionState.Open)
                    connection.Open();

                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Visitas where StartDate between @startDateParam and @endDateParam", connection);
                
                sqlCommand.Parameters.Add("@startDateParam", System.Data.SqlDbType.DateTime).Value = start.ToString("yyyy-MM-dd HH:mm:ss");
                sqlCommand.Parameters.Add("@endDateParam", System.Data.SqlDbType.DateTime).Value = end.ToString("yyyy-MM-dd HH:mm:ss");

                SqlDataReader reader = sqlCommand.ExecuteReader();


                SLDocument sLDocument = new SLDocument();
                int i = getLastIndexExcel(sLDocument);
                while (reader.Read())
                {
                    object[] data = new object[reader.FieldCount];
                    reader.GetValues(data);
                    Visitor v = new Visitor(data[1].ToString(), data[0].ToString(), data[2].ToString(), data[3].ToString(), (DateTime)data[4], (DateTime)data[5], "", false);
                    sLDocument.SetCellValue(i, 1, v.name);
                    sLDocument.SetCellValue(i, 2, v.dni);
                    sLDocument.SetCellValue(i, 3, v.company);
                    sLDocument.SetCellValue(i, 4, v.contact.name);
                    sLDocument.SetCellValue(i, 5, v.startDate);
                    sLDocument.SetCellValue(i, 6, v.finishDate);
                }
                reader.Close();
                sLDocument.SaveAs(outputFile);
                return true;
            }
            catch(Exception ex)
            {

                //System.Windows.Forms.MessageBox.Show("Fallo en el ExportExcel.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public int getLastIndexExcel(SLDocument sLDocument)
        {
            for (int i = 1; i < i + 2; i++)
            {
                if (sLDocument.GetCellValueAsString(i, 1) == "")
                    return i;
            }
            return 0;
        }

        public void updateVisitorAll(Visitor v)
        {
            try
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                String sql = "";
                if(v.finishDate.Date.Ticks!= 0)
                    sql = "Update Visitas SET DNI = '" + v.dni + "' , Name = '" + v.name
                    + "', Company = '" + v.company + "', Contact = '" + v.contact.name + "', StartDate = Convert(datetime, '" + v.startDate.ToString() +
                    "'), FinishDate = Convert(datetime, '" + v.finishDate.ToString() + "') where DNI Like '" + v.dni + "' And StartDate Like '" + v.startDate.ToString() + "'";
                else
                    sql = "Update Visitas SET DNI = '" + v.dni + "' , Name = '" + v.name
                    + "', Company = '" + v.company + "', Contact = '" + v.contact.name + "', StartDate = Convert(datetime, '" + v.startDate.ToString() +
                    "') where DNI Like '" + v.dni + "' And StartDate Like '" + v.startDate.ToString() + "'";

                SqlCommand command = new SqlCommand(sql, connection);
                adapter.UpdateCommand = new SqlCommand(sql, connection);
                adapter.UpdateCommand.ExecuteNonQuery();
            } 
            catch (Exception ex)
            {

                //System.Windows.Forms.MessageBox.Show("Fallo en el updateVisitorAll.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public List<string> getAllActualVisitors()
        {
            try
            {
                List<string> names = new List<string>();
                SqlCommand sqlCommand = new SqlCommand("SELECT * FROM Visitas where StartDate between '" + DateTime.Now.ToShortDateString() + "' and '" + DateTime.Now.ToShortDateString() + " 23:59:59' And FinishDate IS NULL", connection);
                SqlDataReader reader = sqlCommand.ExecuteReader();

                
                while (reader.Read())
                {
                    object[] data = new object[reader.FieldCount];
                    reader.GetValues(data);
                    string item =(string) data[1] + ", con dni: "+(string)data[0] + "de la empresa " + (string)data[2] + ". Su persona de contacto es: " + (string)data[3];
                    names.Add(item);
                }
                reader.Close();
                return names; 
            }
            catch(Exception ex)
            {

                System.Windows.Forms.MessageBox.Show("Fallo en el getAllActualVisitors.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return new List<string>();
            }
        }
    }
}
