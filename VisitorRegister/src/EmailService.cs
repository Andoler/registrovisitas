﻿using Microsoft.Exchange.WebServices.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using VisitorRegister.Common;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace VisitorRegister.src
{
    public class EmailService
    {
        public EmailService()
        {
        }
        public void sendEmail(string from, string to, string body, Visitor v)
        {
            if(SharedVars.config.emailType.Equals("Gmail"))
            {
                sendGmail(from, to, body);
            }
            else
            {
                sendOutlook(from, to, body);
            }
        }

        private void sendOutlook(string from, string to, string body)
        {
            ExchangeService service = new ExchangeService();// ExchangeVersion.Exchange2007_SP1);
            //string user = SharedVars.config.username.Substring(0, SharedVars.config.username.LastIndexOf('@'));
            service.Credentials = new WebCredentials("visitas", "Ekide123", SharedVars.config.domain);//SharedVars.config.username, SharedVars.config.password, SharedVars.config.domain);
            service.AutodiscoverUrl("visitas@ekide.es");
            NameResolutionCollection searchResult = service.ResolveName(to, ResolveNameSearchLocation.DirectoryThenContacts, true);
               
            foreach (NameResolution resolution in searchResult)
            {
                if (resolution.Contact == null)
                    continue;
                EmailMessage email = new EmailMessage(service);
                email.ToRecipients.Add(resolution.Mailbox.Address);
                email.Body = body;
                email.Subject = "Recepcción Visitas";
                email.SendAndSaveCopy();

             }
         }

        private void sendGmail(string from, string to, string body)
        {
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(SharedVars.config.username, SharedVars.config.password),
                Timeout = 10000
            };

            string subject = "Recepción de Visitas";
            //string body = "La visita: " + this.nameTextBox.Text + " le espera en recepción. \n Registrado a las: " + DateTime.Now.ToLongTimeString() + " " + DateTime.Now.ToLongDateString();

            using (var message = new MailMessage(from, to)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }
        }
    }
}
