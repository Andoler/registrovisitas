﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace VisitorRegister.src
{
    public class Configuration
    {
        public string serverConnection { get; set; }
        public string contactPath { get; set; }
        public string emailType { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string domain { get; set; }
        public string Location { get; set; }
        public string contactoAlarma { get; set; }

        public static Configuration LoadConfigXmlFromDisk(String path)
        {
            XmlSerializer xmlSerial = new XmlSerializer(typeof(Configuration));
            Configuration configuration = new Configuration();
            using (XmlReader reader = XmlReader.Create(path))
            {
                configuration = (Configuration)xmlSerial.Deserialize(reader);
            }
            return configuration;
        }

        public void WriteToDisk(String path)
        {
            XmlSerializer xsSubmit = new XmlSerializer(typeof(Configuration));
            var xml = "";

            using (var sww = new StringWriter())
            {
                using (XmlWriter writer = XmlWriter.Create(sww))
                {
                    xsSubmit.Serialize(writer, this);
                    xml = sww.ToString();
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(xml);
                    xdoc.Save(path);
                }
            }
        }
    }
}
